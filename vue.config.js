module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? process.env.CI_PROJECT_PATH ? `/${process.env.CI_PROJECT_PATH.split("/").slice(1).join("/")}` : '/'
    : '/'
}
